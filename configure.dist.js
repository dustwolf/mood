#!/usr/bin/env node

console.log(process.version);

const s = require('node-persist');
try { s.init(); } catch(e) { console.log(e); }

s.setItem('discordToken', ''); //bot authentication token
s.setItem('discordCommandsRoom', ''); //commands room ID
s.setItem('discordVoiceRoom', ''); //voice room ID
