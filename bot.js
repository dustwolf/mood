#!/usr/bin/env node

console.log(process.version);

const EventEmitter = require('events');
const e = new EventEmitter();

const Discord = require('discord.js');
const d = new Discord.Client({ autoReconnect: true });

const s = require('node-persist');
try { s.init(); } catch(e) { console.log(e); }

e.on('startup', async () => {
  console.log('Startup');
  global.voiceRoom = await s.getItem('discordVoiceRoom');
  d.login(await s.getItem('discordToken'));
  setInterval(async () => {e.emit('interval')}, 15*1000);
});

e.on('interval', async () => {
  e.emit('debug', 'interval');
  d.channels.get(global.voiceRoom).members.array().forEach(async (m) => {
    e.emit('upvote', m);
  });
});

async function getPoints(m) {
 let points = parseInt((await s.getItem('points-' + m.displayName)) , 10);
 if(isNaN(points) || typeof points != 'number') { //NaN is a number
   points = 0;
 }
 return points;
}

async function setPoints(m, points) {
 s.setItem('points-' + m.displayName, points);
}

e.on('join', async (m) => {
  e.emit('debug', 'Detected a voice room join');
  let points = await getPoints(m);
  if(points >= 0) {
   global.dCommands.send(':heart:');
  }
});

e.on('upvote', async (m) => {
  let points = await getPoints(m);
  points++;
  setPoints(m, points);
});

e.on('downvote', async (m) => {
  let points = await getPoints(m);
  points--;
  setPoints(m, points);
});

e.on('debug', async (msg) => {
  console.log(msg);
  global.dCommands.send(msg);
});

d.on('ready', async () => {
  console.log('Connected to Discord');
  global.dCommands = d.channels.get(await s.getItem('discordCommandsRoom'));
});

//this will take some doing to merge into the existing bot
d.on('voiceStateUpdate', async (oldMember, newMember) => {
  if(oldMember.voiceChannel === undefined && newMember.voiceChannel !== undefined) {
    if(newMember.voiceChannelID == global.voiceRoom) {
      e.emit('join', newMember);
    }
  }  
});

d.on('message', async msg => {
  let points = await getPoints(msg.member);

  if(msg.content === '&down') {
    e.emit('downvote', msg.member);
  }
  
  if(msg.content === '&points') {
    msg.reply(points);
  }  

  if(msg.content === '&woof') {
    if(points >= 0) {
      msg.reply('bork <:D_:575393714525175826>');
    } else {
      e.emit('debug', 'Refusing command from bad user. <:sad:575393714990481410>');
    }
  }
  
  if(msg.content === '&die') {
    e.emit('debug', 'Shutting down');
    process.exit();
  }
});

d.on('error', async () => {
  console.log('Discord error');
  process.exit();
});

e.emit('startup');
